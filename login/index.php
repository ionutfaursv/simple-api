<?php
require '../db/db.php';

header("Content-Type: application/json");

	if(isset($_POST['user']) && isset($_POST['pass'])) {
		$user = $_POST['user'];
		$pass = $_POST['pass'];

		$id = login($user, $pass);
		if($id){
			response(200, "OK!", $id);

		} else {
			response(401, "Unauthorized", null);
		}

	} else{
		response(400, "Invalid Request", null);
	}
?>
