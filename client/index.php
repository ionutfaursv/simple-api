<?php

//  login
if (isset($_POST['submit_id'])){
    $user = $_POST['user'];
    $pass = $_POST['pass'];

    $data = [
        'user' => $user,
        'pass' => $pass
    ];

    $url = 'http://api.test/login/index.php';

    $client = curl_init($url);
    curl_setopt($client, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($client, CURLOPT_POSTFIELDS, $data);
    curl_setopt($client, CURLOPT_FOLLOWLOCATION, 1);
    $response_id = curl_exec($client);

    $result_id = json_decode($response_id);
}

//  list
if (isset($_POST['submit_list'])){
    $id = $_POST['id'];

    $url = 'http://api.test/list/?user='.$id;

    $client = curl_init($url);
    curl_setopt($client, CURLOPT_RETURNTRANSFER, 1);
    $response_list = curl_exec($client);

    $result_list = json_decode($response_list);
}

//  insert
if (isset($_POST['submit_insert'])){
    $user = $_POST['id'];
    $comment = $_POST['comment'];
    $file = $_POST['file'];

    $data = [
        'user' => $user,
        'comment' => $comment,
        'file' => $file
    ];

    $url = 'http://api.test/insert/index.php';

    $client = curl_init($url);
    curl_setopt($client, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($client, CURLOPT_POSTFIELDS, $data);
    curl_setopt($client, CURLOPT_FOLLOWLOCATION, 1);
    $response_insert = curl_exec($client);

    $result_insert = json_decode($response_insert);
}
?>

<h2>ID</h2>
<form action="" method="POST">
    <label for="user">Username</label>
    <input type="text" name="user">

    <label for="pass">Password</label>
    <input type="text" name="pass">

    <input type="submit" name="submit_id" value="Go!">
</form>
<?php
    if(isset($response_id)){
        echo $response_id.'<br><br>';
    }
?>

<h2>List</h2>
<form action="" method="POST">
    <label for="user">User ID</label>
    <input type="text" name="id">

    <input type="submit" name="submit_list" value="Get list!">
</form>
<?php
    if(isset($response_list)){
        echo $response_list.'<br><br>';
    }
?>

<h2>Insert</h2>
<form action="" method="POST">
    <label for="id">User ID</label>
    <input type="text" name="id">

    <label for="comment">Comment</label>
    <input type="text" name="comment">

    <label for="file">File name</label>
    <input type="text" name="file">

    <input type="submit" name="submit_insert" value="Insert!">
</form>
<?php
if(isset($response_insert)){
    echo $response_insert.'<br><br>';
}
?>

