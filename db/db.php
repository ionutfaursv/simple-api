<?php
$user = "root";
$pass = "parola";

try {
    $pdo = new PDO('mysql:host=localhost;dbname=login', $user, $pass);
} catch (PDOException $e) {
    print "Error!: " . $e->getMessage() . "<br/>";
    die();
}

function login($user, $pass) {
    global $pdo;
    $statement = $pdo->prepare("select id from users where username = :user and password = :pass");

    $statement->bindParam(':user', $user);
    $statement->bindParam(':pass', md5($pass));
    $statement->execute();
    $row = $statement->fetch(PDO::FETCH_ASSOC);

    return $row['id'];
}

function user($id){
    global $pdo;
    $statement = $pdo->prepare("select * from users where id = :id");

    $statement->bindParam(':id', $id);
    $statement->execute();
    $user = $statement->fetch(PDO::FETCH_ASSOC);

    return $user;
}

function addComments($user_id, $comment, $file_name){
    global $pdo;

    $statement = $pdo->prepare("INSERT INTO comments (user_id, comment, file_name) VALUES (:user_id, :comment, :file_name)");
    $row = $statement->execute([
        ':user_id' => $user_id,
        ':comment' => $comment,
        ':file_name' => $file_name
    ]);

    return $row;
}

function getComments($user_id){
    global $pdo;
    $statement = $pdo->prepare("select * from comments where user_id = :user_id");
    $statement->bindParam(':user_id', $user_id);
    $statement->execute();
    $row = $statement->fetchAll(PDO::FETCH_ASSOC);

    return $row;
}


function response($status, $message, $data){
    header("HTTP/1.1 $status $message");
    $response = [
        'status' => $status,
        'message' => $message,
        'data' => $data
    ];

    $json = json_encode($response);

    echo $json;
}

?>

