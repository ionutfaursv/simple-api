<?php
require '../db/db.php';

header("Content-Type: application/json");
if(isset($_GET['user'])) {
    $id = $_GET['user'];

    $user = user($id);

    if($user) {
        $comments = getComments($id);

        if($comments){
            response(200, "OK!", $comments);

        }
    } else {
        response(401, "Unauthorized", null);
    }

} else{
    response(400, "Invalid Request", null);
}
?>
