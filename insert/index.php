<?php
require '../db/db.php';

header("Content-Type: application/json");
if(isset($_POST['user']) & isset($_POST['comment']) && isset($_POST['file'])) {
    $user_id = $_POST['user'];
    $comment = $_POST['comment'];
    $file_name = $_POST['file'];

    $user = user($user_id);

    if($user) {
        $comments = addComments($user_id, $comment, $file_name);

        if($comments){
            response(200, "OK!", $comments);
        }
    } else {
        response(401, "Unauthorized", null);
    }


} else{
    response(400, "Invalid Request", null);
}
?>
